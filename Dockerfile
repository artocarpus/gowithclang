FROM golang:1.18
MAINTAINER Harikrishnan Namboothiri <harikrishnansr92@gmail.com>

ENV GOPATH /go
ENV PATH /go/bin:$PATH
RUN go install golang.org/x/lint/golint@latest; \
    go install -tags 'postgres' github.com/golang-migrate/migrate/v4/cmd/migrate@latest;\
    ls $GOPATH/bin;

# installing requirements to get and extract prebuilt binaries 
RUN apt-get update && apt-get install -y \
    xz-utils \
    curl \
    && rm -rf /var/lib/apt/lists/*

#Getting prebuilt binary from llvm
RUN echo "deb http://apt.llvm.org/bionic/ llvm-toolchain-bionic-6.0 main" >> /etc/apt/source.list; \
    apt-get update && apt-get install -y clang

ENV CC clang
